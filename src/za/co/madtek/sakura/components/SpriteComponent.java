package za.co.madtek.sakura.components;

import org.joml.Vector2f;
import za.co.madtek.sakura.Sakura;
import za.co.madtek.sakura.ecs.Component;
import za.co.madtek.sakura.ecs.Entity;
import za.co.madtek.sakura.graphics.Quad;
import za.co.madtek.sakura.graphics.Shader;
import za.co.madtek.sakura.graphics.Texture2D;

public class SpriteComponent implements Component {

    private Texture2D texture;

    public SpriteComponent(Texture2D texture) {
        this.texture = texture;
    }

    @Override
    public void addedToEntity(Entity parent) {
        //--
    }

    @Override
    public void removedFromEntity(Entity parent) {
        //--
    }

    @Override
    public void update(Entity parent, float delta) {
        //--
    }

    @Override
    public void render(Entity parent) {
        Vector2f size = new Vector2f(texture.width, texture.height).mul(parent.scale);
        Sakura.graphics.renderTexture(texture, parent.position, size, parent.rotation);
    }
}
