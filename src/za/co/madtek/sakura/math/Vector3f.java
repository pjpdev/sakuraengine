package za.co.madtek.sakura.math;

public class Vector3f {

    public float x;
    public float y;
    public float z;

    public Vector3f() {
        this(0.0f);
    }

    public Vector3f(float value) {
        this(value, value, value);
    }

    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
