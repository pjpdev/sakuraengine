package za.co.madtek.sakura.math;

public class Vector2f {

    public float x;
    public float y;

    public Vector2f() {
        this(0.0f);
    }

    public Vector2f(float value) {
        this(value, value);
    }

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
