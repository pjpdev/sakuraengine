/*******************************************************************************************************************
 * Sakura Game Engine
 * Copyright (C) 2020 Madtek Studios. All Rights Reserved.
 *******************************************************************************************************************/

package za.co.madtek.sakura;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import za.co.madtek.sakura.graphics.*;

import org.lwjgl.opengl.*;
import static org.lwjgl.opengl.GL33.*;

public class Graphics {

    private String vShaderCode = "#version 330 core\n" +
            "layout (location = 0) in vec4 vertex; // <vec2 position, vec2 texCoords>\n" +
            "out vec2 TexCoords;\n" +
            "uniform mat4 model;\n" +
            "uniform mat4 projection;\n" +
            "void main() {\n" +
            "    TexCoords = vertex.zw;\n" +
            "    gl_Position = projection * model * vec4(vertex.xy, 0.0, 1.0);}";

    private String fShaderCode = "#version 330 core\n" +
            "in vec2 TexCoords;\n" +
            "out vec4 color;\n" +
            "uniform sampler2D image;\n" +
            "uniform vec3 spriteColor;\n" +
            "void main(){ color = vec4(spriteColor, 1.0) * texture(image, TexCoords); }";

    private Game game;

    private Quad quad;
    private Shader shader;

    public Graphics(Game game) {
        this.game = game;

        quad = new Quad();

        // Compile base shader
        shader = new Shader();
        shader.compile(vShaderCode, fShaderCode);

        Matrix4f projection = new Matrix4f().ortho(0.0f, (float) game.config.screenWidth, (float) game.config.screenHeight, 0.0f, -1.0f, 1.0f);
        shader.setMatrix4("projection", projection, true);
    }

    public void renderTexture(Texture2D texture, Vector2f position, Vector2f scale, float rotation) {
        glBindVertexArray(quad.VAO);
        shader.use();

        Matrix4f model = new Matrix4f();
        model.translate(new Vector3f(position, 0.0f)); // Position

        model.translate(new Vector3f(0.5f * texture.width, 0.5f * texture.height, 0.0f));
        model.rotate(rotation, new Vector3f(0.0f, 0.0f, 1.0f));
        model.translate(new Vector3f(-0.5f * texture.width, -0.5f * texture.height, 0.0f));
        model.scale(new Vector3f(scale, 1.0f)); // Size

        shader.setMatrix4("model", model, false);
        shader.setVector3f("spriteColor", new Vector3f(1.0f, 1.0f, 1.0f), false);

        glActiveTexture(GL_TEXTURE0);
        texture.bind();

        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }
}
