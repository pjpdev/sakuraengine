package za.co.madtek.sakura;

public class Files {

    public String base = "./assets/";

    public Files() {
        this("./assets/");
    }

    public Files(String baseDir) {
        base = baseDir;
    }
}
