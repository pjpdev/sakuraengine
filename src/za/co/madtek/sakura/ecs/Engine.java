package za.co.madtek.sakura.ecs;

import java.util.ArrayList;

public class Engine {

    private ArrayList<Entity> entities;

    public Engine() {
        // Create entity engine
        entities = new ArrayList<Entity>();
    }

    public void add(Entity entity) {
        // Add entity to engine.
        entities.add(entity);
    }

    public Entity get(int id) {
        return entities.get(id);
    }

    public void remove(int id) {
        Entity e = entities.get(id);
        remove(e);
    }

    public void remove(Entity entity) {
        entities.remove(entity);
    }

    public void update(float delta) {
        // Update loop
        for (Entity e : entities) {
            e.update(delta);
        }
    }

    public void render() {
        for (Entity e : entities) {
            e.render();
        }
    }
}
