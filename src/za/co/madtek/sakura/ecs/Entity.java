package za.co.madtek.sakura.ecs;

import org.joml.Vector2f;

import java.util.ArrayList;

public class Entity {

    private ArrayList<Component> components;

    public Vector2f position;
    public Vector2f scale;
    public float rotation;

    public Entity() {
        // Create entity
        components = new ArrayList<Component>();

        position = new Vector2f(0.0f);
        scale = new Vector2f(1.0f);
        rotation = 0.0f;
    }

    public void addComponent(Component component) {
        // Check if component hasn't been added already.
        for (Component c : components) {
            if (c == component) {
                //--
                return;
            }
        }

        components.add(component);
        component.addedToEntity(this);
    }

    public void removeComponent(int id) {
        Component c = components.get(id);
        removeComponent(c);
    }

    public void removeComponent(Component c) {
        components.remove(c);
        c.removedFromEntity(this);
    }

    public Component getComponent(int id) {
        return components.get(id);
    }

    public void update(float delta) {
        // Update loop
        for (Component c : components) {
            c.update(this, delta);
        }
    }

    public void render() {
        // Render loop
        for (Component c : components) {
            c.render(this);
        }
    }

}
