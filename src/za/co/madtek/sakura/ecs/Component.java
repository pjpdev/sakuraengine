package za.co.madtek.sakura.ecs;

public interface Component {

    public void addedToEntity(Entity parent);
    public void removedFromEntity(Entity parent);
    public void update(Entity parent, float delta);
    public void render(Entity parent);
}
