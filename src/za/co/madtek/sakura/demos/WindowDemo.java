package za.co.madtek.sakura.demos;

import za.co.madtek.sakura.*;
import za.co.madtek.sakura.components.SpriteComponent;
import za.co.madtek.sakura.ecs.Engine;
import za.co.madtek.sakura.ecs.Entity;
import za.co.madtek.sakura.graphics.Texture2D;

public class WindowDemo extends Game {

    private Engine engine;

    private Entity sprite;

    @Override
    public void init() {
        engine = new Engine();

        sprite = new Entity();
        sprite.addComponent(new SpriteComponent(new Texture2D("natsu.png")));
        sprite.position.set(10.0f, 10.0f);
        sprite.rotation = 45.0f;

        engine.add(sprite);
    }

    @Override
    public void render() {
        engine.render();
    }

    @Override
    public void update(float delta) {
        if (input.isKeyPressed(Input.KEY_ESCAPE)) {
            terminate();
        }

        sprite.position.x += 50.0f * delta;
        if (sprite.position.x > config.screenWidth) {
            sprite.position.x = -50;
        }

        engine.update(delta);
    }

    @Override
    public void dispose() {

    }

    public static void main(String[] args) {
        WindowDemo app = new WindowDemo();
        app.config.title = "Sakura Engine - Window Demo";
        app.run();
    }
}
