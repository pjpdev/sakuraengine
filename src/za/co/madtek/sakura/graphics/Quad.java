/*******************************************************************************************************************
 * Sakura Game Engine
 * Copyright (C) 2020 Madtek Studios. All Rights Reserved.
 *******************************************************************************************************************/

package za.co.madtek.sakura.graphics;

import org.lwjgl.opengl.*;

import static org.lwjgl.opengl.GL33.*;
import org.joml.*;

public class Quad {

    public int VAO;
    public int VBO;

    private float vertices[] = {
            //Pos       //Tex
            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,

            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f
    };

    public Quad() {
        // Create a quad object.
        VAO = glGenVertexArrays();
        glBindVertexArray(VAO);

        VBO = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, VBO);

        glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 4, GL_FLOAT, false, 4 * Float.BYTES, 0);
        glEnableVertexAttribArray(0);

        // Quad created, clear bindings.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    public void bind() {
        // Bind VAO for use
        glBindVertexArray(VAO);
    }

    public void unbind() {
        // Unbind VAO
        glBindVertexArray(0);
    }

    public void dispose() {
        // Remove VAO and VBO from memory when no longer needed.
        glDeleteBuffers(VBO);
        glDeleteVertexArrays(VAO);
    }
}
