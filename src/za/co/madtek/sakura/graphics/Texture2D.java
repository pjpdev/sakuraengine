/*******************************************************************************************************************
 * Sakura Game Engine
 * Copyright (C) 2020 Madtek Studios. All Rights Reserved.
 *******************************************************************************************************************/

package za.co.madtek.sakura.graphics;

import org.lwjgl.system.MemoryStack;
import za.co.madtek.sakura.Sakura;

import java.nio.*;

import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.stb.STBImage.*;

public class Texture2D {

    public int width;
    public int height;
    public int channels;
    private int id;

    public Texture2D(ByteBuffer imageData) {
        // Get basic image data
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);
            IntBuffer c = stack.mallocInt(1);

            stbi_info(imageData, w, h, c);

            width = w.get(0);
            height = h.get(0);
            channels = c.get(0);
        }

        // Generate Texture
        id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        if (imageData != null) {
            if (channels == 4) {
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
            } else {
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
            }
        }

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public Texture2D(String filename) {
        String fullPath = Sakura.files.base + filename;

        ByteBuffer image;

        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);
            IntBuffer comp = stack.mallocInt(1);

            // Load image
            //stbi_set_flip_vertically_on_load(true);
            image = stbi_load(fullPath, w, h, comp, 0);
            if (image == null) {
                throw new RuntimeException("Failed to load texture!" + System.lineSeparator() + stbi_failure_reason());
            }

            width = w.get(0);
            height = h.get(0);
            channels = comp.get(0);
        }

        // Generate Texture
        id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        if (image != null) {
            if (channels == 4) {
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
            } else {
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
            }
        }

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public void bind() {
        // Bind texture
        glBindTexture(GL_TEXTURE_2D, id);
    }

    public void unbind() {
        // Unbind texture
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public void dispose() {
        // Removes texture from memory when no longer needed.
        glDeleteTextures(id);
    }
}
