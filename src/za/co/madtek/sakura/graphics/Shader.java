/*******************************************************************************************************************
 * Sakura Game Engine
 * Copyright (C) 2020 Madtek Studios. All Rights Reserved.
 *******************************************************************************************************************/

package za.co.madtek.sakura.graphics;

import org.lwjgl.system.*;
import org.joml.*;

import java.nio.*;

import static org.lwjgl.opengl.GL33.*;

public class Shader {

    public int id;

    public void compile(String vertexSource, String fragmentSource) {
        // Compile shaders.
        int sVertex, sFragment;
        // Vertex shader
        sVertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(sVertex, vertexSource);
        glCompileShader(sVertex);
        checkCompileErrors(sVertex, "VERTEX");
        // Fragment shader
        sFragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(sFragment, fragmentSource);
        glCompileShader(sFragment);
        checkCompileErrors(sFragment, "FRAGMENT");

        // Shader program
        id = glCreateProgram();
        glAttachShader(id, sVertex);
        glAttachShader(id, sFragment);
        glLinkProgram(id);
        checkCompileErrors(id, "PROGRAM");

        // Delete shaders, they are already linked.
        glDeleteShader(sVertex);
        glDeleteShader(sFragment);
    }

    public void compile(String vertexSource, String fragmentSource, String geometrySource) {
        // Compile shaders.
        int sVertex, sFragment, gShader;
        // Vertex shader
        sVertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(sVertex, vertexSource);
        glCompileShader(sVertex);
        checkCompileErrors(sVertex, "VERTEX");
        // Fragment shader
        sFragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(sFragment, fragmentSource);
        glCompileShader(sFragment);
        checkCompileErrors(sFragment, "FRAGMENT");
        // Geometry Shader
        gShader = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(gShader, geometrySource);
        glCompileShader(gShader);
        checkCompileErrors(gShader, "GEOMETRY");

        // Shader program
        id = glCreateProgram();
        glAttachShader(id, sVertex);
        glAttachShader(id, sFragment);
        glAttachShader(id, gShader);
        glLinkProgram(id);
        checkCompileErrors(id, "PROGRAM");

        // Delete shaders, they are already linked.
        glDeleteShader(sVertex);
        glDeleteShader(sFragment);
        glDeleteShader(gShader);
    }

    public Shader use() {
        glUseProgram(id);
        return this;
    }

    // Utility
    public void setFloat(String name, float value, boolean useShader) {
        if (useShader) use();
        glUniform1f(glGetUniformLocation(id, name), value);
    }

    public void setInt(String name, int value, boolean useShader) {
        if (useShader) use();
        glUniform1i(glGetUniformLocation(id, name), value);
    }

    public void setVector2f(String name, float x, float y, boolean useShader) {
        setVector2f(name, new Vector2f(x, y), useShader);
    }
    public void setVector2f(String name, Vector2f value, boolean useShader) {
        if (useShader) use();
        glUniform2f(glGetUniformLocation(id, name), value.x, value.y);
    }

    public void setVector3f(String name, float x, float y, float z, boolean useShader) {
        setVector3f(name, new Vector3f(x, y, z), useShader);
    }
    public void setVector3f(String name, Vector3f value, boolean useShader) {
        if (useShader) use();
        glUniform3f(glGetUniformLocation(id, name), value.x, value.y, value.z);
    }

    public void setVector4f(String name, float x, float y, float z, float w, boolean useShader) {
        setVector4f(name, new Vector4f(x, y, z, w), useShader);
    }
    public void setVector4f(String name, Vector4f value, boolean useShader) {
        if (useShader) use();
        glUniform4f(glGetUniformLocation(id, name), value.x, value.y, value.z, value.w);
    }

    public void setMatrix4(String name, Matrix4f value, boolean useShader) {
        if (useShader) use();
        // Some Buffer black magic
        /*float arr[] = new float[16];
        value.get(arr);
        glUniformMatrix4fv(glGetUniformLocation(id, name), false, arr);*/
        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer fb = value.get(stack.mallocFloat(16));
            glUniformMatrix4fv(glGetUniformLocation(id, name), false, fb);
        }
    }

    private void checkCompileErrors(int object, String type) {
        // Check for shader compile errors.
        int success;
        String infoLog;

        if (!type.equals("PROGRAM")) {
            success = glGetShaderi(object, GL_COMPILE_STATUS);
            if (success == 0) {
                infoLog = glGetShaderInfoLog(object, 1024);
                System.out.println("ERROR: Shader failed to compile! Type: " + type);
                System.out.println(infoLog);
            }
        } else {
            success = glGetProgrami(object, GL_LINK_STATUS);
            if (success == 0) {
                infoLog = glGetProgramInfoLog(object, 1024);
                System.out.println("ERROR: Link time error. Type: " + type);
                System.out.println(infoLog);
            }
        }
    }

    public void dispose() {
        // Delete shader program when no longer needed.
        glDeleteProgram(id);
    }
}
