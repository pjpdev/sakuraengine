/*******************************************************************************************************************
 * Sakura Game Engine
 * Copyright (C) 2020 Madtek Studios. All Rights Reserved.
 *******************************************************************************************************************/

package za.co.madtek.sakura;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.nio.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public abstract class Game {

    public Config config;

    protected long windowHandle;

    protected Input input;
    protected Graphics graphics;
    protected Files files;

    public Game() {
        this.config = new Config();
    }

    protected void run() {
        //-------------------------------------
        // Init Display
        //-------------------------------------

        // GLFW Error Callback
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW!");

        // Configure GLFW
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, config.resizeable ? GLFW_TRUE : GLFW_FALSE);

        long fullscreen = config.fuillscreen ? glfwGetPrimaryMonitor() : NULL;

        // Create window
        windowHandle = glfwCreateWindow(config.screenWidth, config.screenHeight, config.title, fullscreen, NULL);
        if (windowHandle == NULL)
            throw new RuntimeException("Failed to create GLFW window!");

        // Get thread stack and push new frame.
        try (MemoryStack stack = stackPush()) {
            IntBuffer pWidth = stack.mallocInt(1);
            IntBuffer pHeight = stack.mallocInt(1);

            // Get window size
            glfwGetWindowSize(windowHandle, pWidth, pHeight);

            // Get the resolution of primary monitor
            GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    windowHandle,
                    (vidMode.width() - pWidth.get(0)) / 2,
                    (vidMode.height() - pHeight.get(0)) / 2
            );
        }

        // Make OpenGL context current.
        glfwMakeContextCurrent(windowHandle);
        //Enable v-sync
        glfwSwapInterval(0);

        // Show the window
        glfwShowWindow(windowHandle);

        // Create OpenGL capabilities
        GL.createCapabilities();
        glViewport(0, 0, config.screenWidth, config.screenHeight);
        glEnable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // Initialize subsystems
        Sakura.game = this;

        input = new Input(this);
        Sakura.input = input;

        graphics = new Graphics(this);
        Sakura.graphics = graphics;

        files = new Files();
        Sakura.files = files;

        // Initialize user code
        init();

        // Delta vars
        float deltaTime;
        float lastTime = System.nanoTime();

        // Main loop
        while (!glfwWindowShouldClose(windowHandle)) {
            // Calculate delta time.
            float currentTime = System.nanoTime();
            deltaTime = (currentTime - lastTime) / 1000000000.0f;
            lastTime = currentTime;

            // Poll events
            glfwPollEvents();

            // Update user code
            update(deltaTime);

            // Render
            glClearColor(0.0f, 0.25f, 0.50f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            render();

            glfwSwapBuffers(windowHandle);
        }

        // Dispose - user code
        dispose();

        // Clear GLFW
        glfwFreeCallbacks(windowHandle);
        glfwDestroyWindow(windowHandle);
        glfwTerminate();
    }

    protected void terminate() {
        glfwSetWindowShouldClose(windowHandle, true);
    }

    public abstract void init();
    public abstract void render();
    public abstract void update(float delta);
    public abstract void dispose();
}
