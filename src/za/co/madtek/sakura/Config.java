package za.co.madtek.sakura;

public class Config {

    public int screenWidth;
    public int screenHeight;
    public boolean fuillscreen;
    public boolean resizeable;
    public String title;

    public Config(String title, int width, int height, boolean fuillscreen, boolean resizeable) {
        this.title = title;
        this.screenWidth = width;
        this.screenHeight = height;
        this.fuillscreen = fuillscreen;
        this.resizeable = resizeable;
    }

    public Config() {
        this.title = "Sakura Engine Application";
        this.screenWidth = 1280;
        this.screenHeight = 720;
        this.fuillscreen = false;
        this.resizeable = false;
    }
}
